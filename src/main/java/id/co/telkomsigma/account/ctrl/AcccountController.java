package id.co.telkomsigma.account.ctrl;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class AcccountController {

	private Random random = new Random();
	
	@GetMapping("/api/account/{nama}")
    public Map<String, String> hitungBiaya(@PathVariable String nama){
        Map<String, String> hasil = new LinkedHashMap<>();
        hasil.put("asal", nama);
        
        hasil.put("reguler", String.valueOf(random.nextInt(100000)+10000));
        hasil.put("kilat", String.valueOf(random.nextInt(200000)+20000));
        return hasil;
    }
}
